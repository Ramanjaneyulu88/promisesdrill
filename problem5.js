/* 
Q5. Promisify the following "sayHelloWorld" function 

    const sayHelloWorld = () => {
        window.setTimeout(() => {
            console.log('Hello World')
        }, 1000)

        return;
    }

    (function executeSayHelloWorld () {
        sayHelloWorld();
        console.log('Hey');
    })()

    Note: You need to execute Question 5 on browser due to window.setTimeout.
    Upon running the function in browser you would notice ..that 
    "Hey" gets printed first and then "Hello World".
    Promisify sayHelloWorld so that.."Hello World" gets printed first and then "Hey".


*/


const sayHelloWorld = () => {

    return new Promise((resolve,reject) => {
        
        
       
        
        setTimeout(() => {
            resolve('Hello World')
        }, 1000)

    })


}

(async function executeSayHelloWorld(){
  await  sayHelloWorld().then(res => console.log(res))
    console.log("hey")
})()




