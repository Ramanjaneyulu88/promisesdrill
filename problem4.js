/* 

Q4. Write a function that takes any number of arguments (userIds) ..
and makes api call to fetch the user details.

        A. use Promises.all and make api call for each user
        B. use only 1 api call to get all the results.
*/

const axios = require("axios");
const fs = require("fs");
const { resolve } = require("path");
const path = require("path");

const usersAPIUrl = "https://jsonplaceholder.typicode.com/users";

const todosApiUrl = "https://jsonplaceholder.typicode.com/todos";

const usersApiUrlSpecificId =
  "https://jsonplaceholder.typicode.com/users?id=2302913&id=2399&id=28193829138";

function fetchUserDetails(...arr) {
  Promise.all(
    arr.map((each) => {
        
     return axios
        .get(`${usersAPIUrl}?id=${each}`)
        .then((res) => res.data)
        .then((data) => data);
    })
  ).then((resultsArr) => console.log(resultsArr));
}

//fetchUserDetails(1, 2, 5);

function fetchUserDetailsAtOnce(...arr){
    let url = usersAPIUrl + `?id=${arr[0]}`
    for(let i = 1; i< arr.length;i ++){
        url = url + `&id=${arr[i]}`
    }

   axios.get(url).then(res => console.log(res.data))
}

fetchUserDetailsAtOnce(1,2,3)

//axios.get(`${usersAPIUrl}?id=${1}`).then((res) => console.log(res.data));
