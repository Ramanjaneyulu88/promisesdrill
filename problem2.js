const axios = require("axios");
const fs = require("fs");
const path = require("path");

const usersAPIUrl = "https://jsonplaceholder.typicode.com/users";

const todosApiUrl = "https://jsonplaceholder.typicode.com/todos";

/* 

Q2. Write a method that returns a promise to read a File (use fs.readFile inside). 
        your method readFile should use fs.readFile and return a promise.
        your method readFile should accept a file path.
        your method should throw an error with message saying "Missing File." if path is incorrect.
        your method should also accept another optional params for transformation of the data .        
       
        write a method for Transformation of data after read operation ...in the form [solutionA, solutionB, solutionC, solutionD].
 */

function readFile(path, options) {
  return new Promise((resolve, reject) => {
    if (!path) {
      reject("Missing file");
    } else {
      fs.readFile(path, (err, data) => {
        if (err) throw err;
        resolve(data);
      });
    }
  });
}

//

readFile("solution1.json", {})
  .then((data) => JSON.parse(data))
  .then((finalObj) => console.log(Object.values(finalObj)));
