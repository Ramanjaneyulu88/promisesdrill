const axios = require("axios");
const fs = require("fs");
const path = require("path");

const usersAPIUrl = "https://jsonplaceholder.typicode.com/users";

const todosApiUrl = "https://jsonplaceholder.typicode.com/todos";

/* 
Q1.Write a function to fetch list of all todos from the above url using both fetch and axios.get.
		
	A. use promises 
	B. use await keyword 	
	
    C. Once the list is fetched.Group the list of tasks based on user IDs.
        Make sure the non completed tasks are always in front.
	{ 
		userId1: [ // All the tasks of userId1]
        	,
		userId2: [ // All the tasks of userId2]

        ...
    
    D.  Also Group tasks based on completed or nonCompleted for each user
        { 
          completed: [..All the completed tasks],
          nonCompleted: [...All the non completed tasks]
        }

    E. Dump the results in a file.

        {
            A: solutionA,
            B: solutionB,
            c: solutionC,
            ...
        }

*/

axios
  .get(todosApiUrl)
  .then((response) => response.data)
  .then((data) => {
    console.log(data[0]);

    const dataGrouptBasedID = groupBasedOnId(data);

    const groutDataCompletNoncomplete = groupCompletedAndNoncompleted(data);

    let resultObj = {};
    resultObj["A"] = data;
    resultObj["B"] = dataGrouptBasedID;
    resultObj["C"] = groutDataCompletNoncomplete;
    dumpToFiles(resultObj);
  });

function groupBasedOnId(arr) {
  return arr.reduce((acc, eachUser) => {
    if (acc[`userId ${eachUser["userId"]}`]) {
      acc[`userId ${eachUser["userId"]}`].push(eachUser["title"]);
    } else {
      acc[`userId ${eachUser["userId"]}`] = [];
      acc[`userId ${eachUser["userId"]}`].push(eachUser["title"]);
    }

    return acc;
  }, {});
}

// {
//     completed: [..All the completed tasks],
//     nonCompleted: [...All the non completed tasks]
//   }

function groupCompletedAndNoncompleted(arr) {
  return arr.reduce((acc, eachUser) => {
    if (acc[`userId ${eachUser["userId"]}`]) {
      let user = acc[`userId ${eachUser["userId"]}`];

      if (eachUser["completed"]) {
        user["completed"].push(eachUser["title"]);
      } else {
        user["nonCompleted"].push(eachUser["title"]);
      }
    } else {
      acc[`userId ${eachUser["userId"]}`] = {};

      let user = acc[`userId ${eachUser["userId"]}`];

      user["completed"] = [];
      user["nonCompleted"] = [];

      if (eachUser["completed"]) {
        user["completed"].push(eachUser["title"]);
      } else {
        user["nonCompleted"].push(eachUser["title"]);
      }
    }

    return acc;
  }, {});
}

function dumpToFiles(resultObj) {
  //console.log(args)

  let stringifiedData = JSON.stringify(resultObj);
  fs.writeFile(
    path.join(__dirname, `solution1.json`),
    stringifiedData,
    function (err) {
      if (err) {
        throw err;
      } else {
        console.log(`Dumped to solutions1`);
      }
    }
  );
}
