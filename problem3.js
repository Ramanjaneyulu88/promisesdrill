
const axios = require("axios");
const fs = require("fs");
const { resolve } = require("path");
const path = require("path");

const usersAPIUrl = "https://jsonplaceholder.typicode.com/users";

const todosApiUrl = "https://jsonplaceholder.typicode.com/todos";

const usersApiUrlSpecificId =
  "https://jsonplaceholder.typicode.com/users?id=2302913&id=2399&id=28193829138";

 
/* Q3. Write a function to get all the users information from the users API url. Find all the users with name "Nicholas". 
Get all the to-dos for those user ids.

{ userId: 1, id: 1, title: 'delectus aut autem', completed: false }
*/ 

axios
  .get(usersAPIUrl)
  .then((response) => response.data)
  .then((data) => {
    let usersIdData = getDataOfSpecificUsers(data, "Nicholas");
    //console.log(usersIdData)
    axios
      .get(todosApiUrl)
      .then((response) => response.data)
      .then((todoData) => {
        // console.log(todoData[0])
        let todoDataOfSpecificId = todoOfSpecificId(todoData, usersIdData);
        console.log(todoDataOfSpecificId);
      });
  });

function getDataOfSpecificUsers(arr, userName) {
  return arr.reduce((acc, each) => {
    if (each["name"].indexOf(userName) !== -1) {
      acc[`userId ${each["id"]}`] = each["id"];
    }
    return acc;
  }, {});
}

function todoOfSpecificId(todoData, usersIdData) {
  return todoData.reduce((acc, each) => {
    if (usersIdData[`userId ${each["userId"]}`]) {
      if (acc[each["userId"]]) {
        acc[each["userId"]].push(each["title"]);
      } else {
        acc[each["userId"]] = [];
        acc[each["userId"]].push(each["title"]);
      }
    }
    return acc;
  }, {});
}

